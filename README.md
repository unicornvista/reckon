# Reckon

A simple, command-line oriented score keeping program suitable for tracking dice or card games. Also runs well on some mobile python interpreters.

## Installation

Use the `requirements.txt` file to install dependencies in your interpreter or virtual environment.

## Usage

Within the project directory and with an interpreter that has required packages, run:

```
$ python main.py
```

To start a new game, use the command:

```
> newgame Rummy
```

You may then add players with the following command:

```
(Rummy) > addplayer John
(Rummy) > addplayer Jane
```

Should you need to remove a player use:

```
(Rummy) > remplayer John
```

To alter the score of a player, use `add`, `sub` or `set` to add, subtract or assign values.

```
(Rummy) > add John 100
(Rummy) > sub John 50
(Rummy) > set John 1000
```

To view a player's status, including their score history, use `player`. To view the game status use `game`. You may toggle whether **Reckon** auto shows the game status after certain commads using the `autoshow` command.

Use the in-game help on each of these commands to learn about optional paramters that some commands take to override default scores and add notes to score alterations.

## Authors

Benjamin Gates

## License

MIT

## Project status

Permanent beta.
