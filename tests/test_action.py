from unittest import TestCase
from reckon import Act, Action

DEF_ACT = Act.ADD
DEF_INT = 100
DEF_STR = "A"

NOTE = "Note"


class TestAction(TestCase):
    # ==========================================================[ __init__() ]==

    def test_init(self):
        with self.assertRaises(TypeError):
            Action("invalid", DEF_INT)

    # =================================================================[ act ]==

    def test_act(self):
        action = Action(Act.ADD, DEF_INT)
        self.assertEqual("+", action.act.value)

        action = Action(Act.SUBTRACT, DEF_INT)
        self.assertEqual("-", action.act.value)

        action = Action(Act.ASSIGN, DEF_INT)
        self.assertEqual("=", action.act.value)

    # ===============================================================[ value ]==

    def test_value(self):
        action = Action(DEF_ACT, 100)
        self.assertEqual(100, action.value)

        action = Action(DEF_ACT, 200)
        self.assertEqual(200, action.value)

    # ================================================================[ note ]==

    def test_note(self):
        action = Action(DEF_ACT, DEF_INT)
        self.assertIsNone(action.note)

        action = Action(DEF_ACT, DEF_INT, NOTE)
        self.assertEqual(NOTE, action.note)

    # ============================================================[ __eq__() ]==

    def test_eq(self):
        action = Action(Act.ADD, 100)
        noted_action = Action(Act.ADD, 100)

        action_ss = Action(Act.ADD, 100)
        action_ds = Action(Act.SUBTRACT, 100)
        action_sd = Action(Act.ADD, 200)
        action_dd = Action(Act.SUBTRACT, 200)

        noted_action_ss = Action(Act.ADD, 100, NOTE)
        noted_action_ds = Action(Act.SUBTRACT, 100, NOTE)
        noted_action_sd = Action(Act.ADD, 200, NOTE)
        noted_action_dd = Action(Act.SUBTRACT, 200, NOTE)

        self.assertTrue(action == action_ss)
        self.assertTrue(action_ss == action)
        self.assertTrue(action == noted_action_ss)
        self.assertTrue(noted_action_ss == action)
        self.assertTrue(noted_action == action_ss)
        self.assertTrue(action_ss == noted_action)
        self.assertTrue(noted_action == noted_action_ss)
        self.assertTrue(noted_action_ss == noted_action)

        self.assertFalse(action == action_ds)
        self.assertFalse(action_ds == action)
        self.assertFalse(action == noted_action_ds)
        self.assertFalse(noted_action_ds == action)
        self.assertFalse(noted_action == action_ds)
        self.assertFalse(action_ds == noted_action)
        self.assertFalse(noted_action == noted_action_ds)
        self.assertFalse(noted_action_ds == noted_action)

        self.assertFalse(action == action_sd)
        self.assertFalse(action_sd == action)
        self.assertFalse(action == noted_action_sd)
        self.assertFalse(noted_action_sd == action)
        self.assertFalse(noted_action == action_sd)
        self.assertFalse(action_sd == noted_action)
        self.assertFalse(noted_action == noted_action_sd)
        self.assertFalse(noted_action_sd == noted_action)

        self.assertFalse(action == action_dd)
        self.assertFalse(action_dd == action)
        self.assertFalse(action == noted_action_dd)
        self.assertFalse(noted_action_dd == action)
        self.assertFalse(noted_action == action_dd)
        self.assertFalse(action_dd == noted_action)
        self.assertFalse(noted_action == noted_action_dd)
        self.assertFalse(noted_action_dd == noted_action)

        self.assertFalse(action == "action")
        self.assertFalse("action" == action)
        self.assertFalse(action == 1)
        self.assertFalse(1 == action)
        self.assertFalse(action == [])
        self.assertFalse([] == action)
