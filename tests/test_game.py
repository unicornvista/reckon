from unittest import TestCase
from reckon import Game

GAME = "Game"
PLAYER = "Player"


class TestGame(TestCase):
    # ==========================================================[ __init__() ]==

    def test_init(self):
        game = Game(GAME)
        self.assertEqual(GAME, game.name)
        self.assertIsNone(game.start)

        game = Game(GAME, 100)
        self.assertEqual(GAME, game.name)
        self.assertEqual(100, game.start)

    # ===============================================================[ add() ]==

    def test_add(self):
        game = Game(GAME)
        self.assertEqual(0, len(game.players))

        game.add(PLAYER)
        self.assertEqual(1, len(game.players))
        player = game.players[0]
        self.assertEqual(PLAYER, player.name)
        self.assertEqual(0, player.score.total)

        game = Game(GAME)
        game.add(PLAYER)
        self.assertEqual(0, game[PLAYER].score.total)

        game = Game(GAME, 100)
        game.add(PLAYER)
        self.assertEqual(100, game[PLAYER].score.total)

        game = Game(GAME)
        game.add(PLAYER, 900)
        self.assertEqual(900, game[PLAYER].score.total)

        game = Game(GAME, 100)
        game.add(PLAYER, 900)
        self.assertEqual(900, game[PLAYER].score.total)

        with self.assertRaises(ValueError):
            game = Game(GAME)
            game.add(PLAYER)
            game.add(PLAYER)

    # =======================================================[ __getitem__() ]==

    def test_getitem(self):
        game = Game(GAME)
        game.add(PLAYER)
        self.assertEqual(1, len(game.players))

        player1 = game.players[0]
        player2 = game[PLAYER]
        self.assertEqual(player1, player2)

    # =======================================================[ __delitem__() ]==

    def test_delitem(self):
        game = Game(GAME)
        game.add(PLAYER)
        self.assertEqual(1, len(game.players))

        del game[PLAYER]
        self.assertEqual(0, len(game.players))
