from unittest import TestCase
from reckon import Act, Action, Score

NOTE = "Note"


class TestScore(TestCase):
    # ==========================================================[ __init__() ]==

    def test_init(self):
        score = Score()
        self.assertEqual(0, score.total)
        self.assertEqual(Action(Act.ASSIGN, 0), score.history[0])

        score = Score(100)
        self.assertEqual(100, score.total)
        self.assertEqual(Action(Act.ASSIGN, 100), score.history[0])

    # ==========================================================[ __iadd__() ]==

    def test_iadd(self):
        score = Score()
        score += 100
        self.assertEqual(100, score.total)
        self.assertEqual(Action(Act.ASSIGN, 0), score.history[0])
        self.assertEqual(Action(Act.ADD, 100), score.history[1])
        self.assertIsNone(score.history[1].note)

        score += 900, NOTE
        self.assertEqual(1000, score.total)
        self.assertEqual(Action(Act.ADD, 900), score.history[2])
        self.assertEqual(NOTE, score.history[2].note)

        with self.assertRaises(TypeError):
            score += "A"
        with self.assertRaises(IndexError):
            score += ()
        with self.assertRaises(IndexError):
            score += (100,)
        with self.assertRaises(TypeError):
            score += []

        score = Score(100)
        score += 100
        self.assertEqual(200, score.total)

    # ==========================================================[ __isub__() ]==

    def test_isub(self):
        score = Score()
        score -= 100
        self.assertEqual(-100, score.total)
        self.assertEqual(Action(Act.ASSIGN, 0), score.history[0])
        self.assertEqual(Action(Act.SUBTRACT, 100), score.history[1])
        self.assertIsNone(score.history[1].note)

        score -= 900, NOTE
        self.assertEqual(-1000, score.total)
        self.assertEqual(Action(Act.SUBTRACT, 900), score.history[2])
        self.assertEqual(NOTE, score.history[2].note)

        with self.assertRaises(TypeError):
            score -= "A"
        with self.assertRaises(IndexError):
            score -= ()
        with self.assertRaises(IndexError):
            score -= (100,)
        with self.assertRaises(TypeError):
            score -= []

        score = Score(100)
        score -= 100
        self.assertEqual(0, score.total)

    # ===========================================================[ __ior__() ]==

    def test_ior(self):
        score = Score()
        score |= 100
        self.assertEqual(100, score.total)
        self.assertEqual(Action(Act.ASSIGN, 0), score.history[0])
        self.assertEqual(Action(Act.ASSIGN, 100), score.history[1])
        self.assertIsNone(score.history[1].note)

        score |= 900, NOTE
        self.assertEqual(900, score.total)
        self.assertEqual(Action(Act.ASSIGN, 900), score.history[2])
        self.assertEqual(NOTE, score.history[2].note)

        score |= -900
        self.assertEqual(-900, score.total)

        with self.assertRaises(IndexError):
            score |= ()
        with self.assertRaises(IndexError):
            score |= (100,)

        score = Score(100)
        score |= 200
        self.assertEqual(200, score.total)

    # ============================================================[ __eq__() ]==

    def test_eq(self):
        score = Score(100)

        for x in [100, Score(100)]:
            self.assertTrue(score == x)
            self.assertTrue(x == score)

        for x in [0, Score(0), "0", "100", "A", ()]:
            self.assertFalse(score == x)
            self.assertFalse(x == score)

    # ============================================================[ __ne__() ]==

    def test_ne(self):
        score = Score(100)

        for x in [100, Score(100)]:
            self.assertFalse(score != x)
            self.assertFalse(x != score)

        for x in [0, Score(0), "0", "100", "A", ()]:
            self.assertTrue(score != x)
            self.assertTrue(x != score)

    # ============================================================[ __gt__() ]==

    def test_gt(self):
        score = Score(100)

        self.assertTrue(score > 99)
        self.assertTrue(99 < score)
        self.assertFalse(score > 100)
        self.assertFalse(100 < score)
        self.assertFalse(score > 101)
        self.assertFalse(101 < score)

        self.assertTrue(score > Score(99))
        self.assertFalse(Score(99) > score)
        self.assertFalse(score > Score(100))
        self.assertFalse(Score(100) > score)
        self.assertFalse(score > Score(101))
        self.assertTrue(Score(101) > score)

        with self.assertRaises(TypeError):
            score > "A"
        with self.assertRaises(TypeError):
            "A" < score
        with self.assertRaises(TypeError):
            score > ()
        with self.assertRaises(TypeError):
            () < score

    # ============================================================[ __lt__() ]==

    def test_lt(self):
        score = Score(100)

        self.assertFalse(score < 99)
        self.assertFalse(99 > score)
        self.assertFalse(score < 100)
        self.assertFalse(100 > score)
        self.assertTrue(score < 101)
        self.assertTrue(101 > score)

        self.assertFalse(score < Score(99))
        self.assertTrue(Score(99) < score)
        self.assertFalse(score < Score(100))
        self.assertFalse(Score(100) < score)
        self.assertTrue(score < Score(101))
        self.assertFalse(Score(101) < score)

        with self.assertRaises(TypeError):
            score < "A"
        with self.assertRaises(TypeError):
            "A" > score
        with self.assertRaises(TypeError):
            score < ()
        with self.assertRaises(TypeError):
            () > score

    # ============================================================[ __ge__() ]==

    def test_ge(self):
        score = Score(100)

        self.assertTrue(score >= 99)
        self.assertTrue(99 <= score)
        self.assertTrue(score >= 100)
        self.assertTrue(100 <= score)
        self.assertFalse(score >= 101)
        self.assertFalse(101 <= score)

        self.assertTrue(score >= Score(99))
        self.assertFalse(Score(99) >= score)
        self.assertTrue(score >= Score(100))
        self.assertTrue(Score(100) >= score)
        self.assertFalse(score >= Score(101))
        self.assertTrue(Score(101) >= score)

        with self.assertRaises(TypeError):
            score >= "A"
        with self.assertRaises(TypeError):
            "A" <= score
        with self.assertRaises(TypeError):
            score >= ()
        with self.assertRaises(TypeError):
            () <= score

    # ============================================================[ __le__() ]==

    def test_le(self):
        score = Score(100)

        self.assertFalse(score <= 99)
        self.assertFalse(99 >= score)
        self.assertTrue(score <= 100)
        self.assertTrue(100 >= score)
        self.assertTrue(score <= 101)
        self.assertTrue(101 >= score)

        self.assertFalse(score <= Score(99))
        self.assertTrue(Score(99) <= score)
        self.assertTrue(score <= Score(100))
        self.assertTrue(Score(100) <= score)
        self.assertTrue(score <= Score(101))
        self.assertFalse(Score(101) <= score)

        with self.assertRaises(TypeError):
            score <= "A"
        with self.assertRaises(TypeError):
            "A" >= score
        with self.assertRaises(TypeError):
            score <= ()
        with self.assertRaises(TypeError):
            () >= score
