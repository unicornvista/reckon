from unittest import TestCase
from reckon import Act, Action, Player

PLAYER = "Player"


class TestPlayer(TestCase):
    # ==========================================================[ __init__() ]==

    def test_init(self):
        player = Player(PLAYER)
        self.assertEqual(PLAYER, player.name)
        self.assertEqual(0, player.score.total)

        player = Player(PLAYER, 100)
        self.assertEqual(PLAYER, player.name)
        self.assertEqual(100, player.score.total)

    # ===============================================================[ score ]==

    def test_score(self):
        player = Player(PLAYER)
        player.score += 100
        player.score -= 50
        player.score |= 1000

        self.assertEqual(1000, player.score.total)
        self.assertEqual(Action(Act.ASSIGN, 0), player.score.history[0])
        self.assertEqual(Action(Act.ADD, 100), player.score.history[1])
        self.assertEqual(Action(Act.SUBTRACT, 50), player.score.history[2])
        self.assertEqual(Action(Act.ASSIGN, 1000), player.score.history[3])
