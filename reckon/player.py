from .score import Score


class Player:
    """
    A `Player` is used to bind a 'name' to a 'score'.
    """

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def __init__(self, name: str, start: int = None):
        """
        Creates a new instance with the given 'name' and an optional 'start'
        value.

        :param name: The given name
        :param start: An optional starting value
        """

        self._name = name
        self._score = Score(start) if start else Score()

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @property
    def name(self) -> str:
        """
        The name of this `Player`.

        :return: The name
        """

        return self._name

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @property
    def score(self) -> Score:
        """
        The score of this `Player`.

        :return: The score
        """

        return self._score

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @score.setter
    def score(self, score: Score):
        """
        Sets the score of this `Player`.

        :param score: The new `Score`
        """

        self._score = score
