from typing import Any
from enum import Enum


class Act(Enum):
    """
    An `Act` is the method by which a 'value' is applied.
    """

    ADD = "+"
    SUBTRACT = "-"
    ASSIGN = "="


class Action:
    """
    An `Action` represents a value applied to a score in a particular manner.
    The `Action`'s 'act' describes that manner. An optional note can describe
    the reason behind the action.
    """

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def __init__(self, act: Act, value: int, note: str = None):
        """
        Creates a new instance with a given 'act' and 'value' and an optional
        'note'.

        :param act: The specifc `Act`
        :param value: The value applied
        :param note: An optional, brief explanation
        """

        if not isinstance(act, Act):
            raise TypeError("Parameter 'act' must be instance of 'Act'")

        self._act = act
        self._value = value
        self._note = note

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @property
    def act(self) -> Act:
        """
        The specific `Act` of this `Action`.

        :return: The specific `Act`
        """

        return self._act

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @property
    def value(self) -> int:
        """
        The value applied by this `Action`.

        :return: The applied value
        """

        return self._value

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @property
    def note(self) -> str:
        """
        An optional, brief explanation of this `Action`.

        :return: An optional, brief explanation
        """

        return self._note

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def __eq__(self, other: Any) -> bool:
        """
        Determines if 'other' is equal to this instance based on 'act' and
        'value'. The 'note' property does not factor into equality.

        :param other: The other value (`Action`)

        :return: Whether 'other' is equal to this instance
        """

        if isinstance(other, Action):
            return (self._act == other._act) and (self._value == other._value)
        return NotImplemented
