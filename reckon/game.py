from .player import Player


class Game:
    """
    A `Game` is a named instance which contains a list of players.
    """
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def __init__(self, name: str, start: int = None):
        self._name = name
        self._start = start
        self._players = []

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @property
    def name(self) -> str:
        return self._name

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @property
    def start(self) -> int:
        return self._start

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @property
    def players(self) -> list[Player]:
        return self._players[:]

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def __contains__(self, item: str) -> bool:
        for player in self._players:
            if player.name == item:
                return True
        return False

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def __getitem__(self, key: str) -> Player:
        for player in self._players:
            if player.name == key:
                return player
        raise KeyError(key)

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def __delitem__(self, key: str):
        player = self[key]
        self._players.remove(player)

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def add(self, name: str, start: int = None):
        if name in self:
            raise ValueError(f"Player '{name}' already exists.")
        player = Player(name, start or self.start)
        self._players.append(player)
