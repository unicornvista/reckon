import json

from rich.console import Console
from .game import Game


class View:
    ERR_EXPECTED_NAME_SCORE = "Expected: <name> [<score>]"
    ERR_EXPECTED_NAME_VALUE_NOTE = "Expected: <name> <value> [<note>]"
    ERR_NEW_GAME = "You need to start a new game first."

    MSG_QUIT = "[bright_red]Bye![/bright_red]"
    MSG_ERROR = "[red]Error:[/red]\n  [yellow]{}[/yellow]"
    MSG_AUTO_SHOW = "[white]autoshow:[/white] [cyan]{}[/cyan]"

    HELP_USAGE = "[cyan]Usage:[/cyan] [green]{}[/green]"
    HELP_DESCRIPTION = "[white]{}[/white]"
    HELP_PARAMETER = "  [magenta]<{}>[/magenta] [white]{}[/white]"

    GAME_HEADER = "[magenta]Game: {}[/magenta] - [yellow]{} Players[/yellow]"
    GAME_PLAYER = "  [green]{}[/green]: [cyan]{}[/cyan]"

    PLAYER_HEADER = "[green]Player: {}[/green] - [cyan]Score: {}[/cyan]"
    PLAYER_HISTORY = "  [green]{}[/green] [cyan]{}[/cyan]"
    PLAYER_HISTORY_N = "  [green]{}[/green] [cyan]{}[/cyan] [yellow]'{}'[/yellow]"

    START_PROMPT = "[white]>[white] "
    FULL_PROMPT = "[cyan]({})[/cyan] [white]>[/white] "

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def __init__(self, console: Console):
        self._console = console
        self._game = None
        self._help = json.load(open("help.json"))

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @property
    def console(self) -> Console:
        return self._console

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @property
    def game(self) -> Game:
        return self._game

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @game.setter
    def game(self, game: Game):
        self._game = game

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def show_game(self):
        self.console.print()
        if self._game:
            self.console.rule(
                View.GAME_HEADER.format(self.game.name, len(self.game.players)),
                style="bright_magenta"
            )

            for player in self.game.players:
                self.console.print(
                    View.GAME_PLAYER.format(player.name.title(), player.score.total)
                )

            self.console.rule(style="magenta")
            self.console.print()

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def show_player(self, name):
        self.console.print()
        if self._game:
            player = self._game[name]
            self.console.rule(
                View.PLAYER_HEADER.format(player.name.title(), player.score.total),
                style="bright_green"
            )

            for action in player.score.history:
                act = action.act.value
                val = action.value
                note = action.note

                if note:
                    self.console.print(View.PLAYER_HISTORY_N.format(act, val, note))
                else:
                    self.console.print(View.PLAYER_HISTORY.format(act, val))

            self.console.rule(style="green")
            self.console.print()

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def show_error(self, error: str):
        self.console.print(View.MSG_ERROR.format(error))

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def show_help(self, topic: str):
        data = self._help[topic]
        self.console.print()
        self.console.print(View.HELP_USAGE.format(data["usage"]))
        self.console.print(View.HELP_DESCRIPTION.format(data["description"]))
        if "parameters" in data:
            for param, desc in data["parameters"].items():
                self.console.print(View.HELP_PARAMETER.format(param, desc))
        self.console.print()
