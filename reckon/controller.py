from rich.console import Console
from .cmd import RichCmd
from .view import View
from .game import Game


class Controller(RichCmd):
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def __init__(self):
        super().__init__(Console())
        self.intro = "Welcome to Reckon!"

        self._view = View(self.console)
        self._game: Game | None = None
        self._auto_show = True

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @property
    def prompt(self):
        if self._game:
            return View.FULL_PROMPT.format(self._game.name)
        else:
            return View.START_PROMPT

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @staticmethod
    def _parse_name_score(line: str) -> tuple[str, int | None]:
        if not line:
            raise ValueError(View.ERR_EXPECTED_NAME_SCORE)

        args = line.split(" ")

        if len(args) == 1:
            return args[0], None

        return args[0], int(args[1])

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @staticmethod
    def _parse_name_value_note(line):
        if not line:
            raise ValueError(View.ERR_EXPECTED_NAME_VALUE_NOTE)

        args = line.split(" ")

        if len(args) < 2:
            raise ValueError(View.ERR_EXPECTED_NAME_VALUE_NOTE)

        if len(args) == 2:
            return args[0], int(args[1]), None

        return args[0], int(args[1]), " ".join(args[2:])

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def auto_show_game(self):
        if self._auto_show:
            self._view.show_game()

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def do_quit(self, line: str) -> bool:
        self.console.print(View.MSG_QUIT)
        return True

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def do_autoshow(self, line: str) -> bool:
        self._auto_show = not self._auto_show
        self.console.print(View.MSG_AUTO_SHOW.format(self._auto_show))
        return False

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def do_newgame(self, line: str) -> bool:
        try:
            name, score = self._parse_name_score(line)
            self._game = Game(name, score)
            self._view.game = self._game
        except ValueError as e:
            self._view.show_error(str(e))

        self.auto_show_game()
        return False

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def do_addplayer(self, line: str) -> bool:
        if not self._game:
            self._view.show_error(View.ERR_NEW_GAME)
        else:
            try:
                name, score = self._parse_name_score(line)
                self._game.add(name.lower(), score)
            except ValueError as e:
                self._view.show_error(str(e))

        self.auto_show_game()
        return False

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def do_remplayer(self, line: str) -> bool:
        if not self._game:
            self._view.show_error(View.ERR_NEW_GAME)
        else:
            try:
                del self._game[line.lower()]
            except ValueError as e:
                self._view.show_error(str(e))
            except KeyError as e:
                self._view.show_error(str(e))

        self.auto_show_game()
        return False

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def do_game(self, line: str) -> bool:
        if not self._game:
            self._view.show_error(View.ERR_NEW_GAME)
        else:
            self._view.show_game()

        return False

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def do_player(self, line: str) -> bool:
        if not self._game:
            self._view.show_error(View.ERR_NEW_GAME)
        else:
            self._view.show_player(line.lower())

        self.auto_show_game()
        return False

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def do_add(self, line: str) -> bool:
        if not self._game:
            self._view.show_error(View.ERR_NEW_GAME)
        else:
            try:
                name, value, note = self._parse_name_value_note(line)
                self._game[name.lower()].score += value, note
            except ValueError as e:
                self._view.show_error(str(e))
            except KeyError as e:
                self._view.show_error(f"No such player: {e}")

        self.auto_show_game()
        return False

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def do_sub(self, line: str) -> bool:
        if not self._game:
            self._view.show_error(View.ERR_NEW_GAME)
        else:
            try:
                name, value, note = self._parse_name_value_note(line)
                self._game[name.lower()].score -= value, note
            except ValueError as e:
                self._view.show_error(str(e))
            except KeyError as e:
                self._view.show_error(f"No such player: {e}")

        self.auto_show_game()
        return False

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def do_set(self, line: str) -> bool:
        if not self._game:
            self._view.show_error(View.ERR_NEW_GAME)
        else:
            try:
                name, value, note = self._parse_name_value_note(line)
                self._game[name.lower()].score |= value, note
            except ValueError as e:
                self._view.show_error(str(e))
            except KeyError as e:
                self._view.show_error(f"No such player: {e}")

        self.auto_show_game()
        return False

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def help_help(self):
        self._view.show_help("help")

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def help_quit(self):
        self._view.show_help("quit")

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def help_autoshow(self):
        self._view.show_help("autoshow")

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def help_newgame(self):
        self._view.show_help("newgame")

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def help_addplayer(self):
        self._view.show_help("addplayer")

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def help_remplayer(self):
        self._view.show_help("remplayer")

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def help_game(self):
        self._view.show_help("game")

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def help_player(self):
        self._view.show_help("player")

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def help_add(self):
        self._view.show_help("add")

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def help_sub(self):
        self._view.show_help("sub")

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def help_set(self):
        self._view.show_help("set")
