from rich.console import Console
import string


class RichCmd:
    IDENT_CHARS = string.ascii_letters + string.digits + "_"
    EOF = "EOF"
    DEF_PROMPT = "> "

    # External messages; these are exportred via exceptions
    ERR_NOT_STRING = "items[i] not a string where i in ({})"

    # Internal messages
    MSG_ERR_BAD_COMMAND = "Unknown syntax: '{}'"
    MSG_ERR_NO_HELP = "No help on: '{}'"
    MSG_EMPTY = "<empty>"

    # Command and Help method name prefixes
    PREFIX_COMMAND = "do_"
    PREFIX_HELP = "help_"

    # The default color styles for the internal messages
    DEFAULT_STYLES = {
        "intro": "cyan",
        "help-header": "bright_white",
        "help-doc": "green",
        "help-misc": "cyan",
        "help-undoc": "yellow",
        "error": "red",
        "warn": "yellow"
    }

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def __init__(self, console: Console, styles: dict[str, str] = None):
        self._console = console
        self._last_command = ""
        self._styles = RichCmd.DEFAULT_STYLES.copy()

        if styles:
            self.styles = styles

        self.intro = None
        self.help_header = None
        self.help_ruler = "="
        self.help_doc = "Commands (Type 'help <topic>')"
        self.help_misc = "Miscellaneous Help Topics"
        self.help_undoc = "Undocumented Commands"

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @property
    def console(self) -> Console:
        return self._console

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @property
    def prompt(self) -> str:
        return RichCmd.DEF_PROMPT

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @property
    def supports_shell(self) -> bool:
        return hasattr(self, "do_shell")

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @property
    def styles(self) -> dict[str, str]:
        return self._styles

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @styles.setter
    def styles(self, merge: dict[str, str]):
        for style in self._styles.keys():
            if style in merge:
                self._styles[style] = merge[style]

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @styles.deleter
    def styles(self):
        self._styles = RichCmd.DEFAULT_STYLES.copy()

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def cmdloop(self, intro: str = None):
        self.preloop()

        self.intro = intro or self.intro

        if self.intro:
            self.console.print(self.intro, style=self.styles["intro"])

        stop = False

        while not stop:
            try:
                self.console.print(self.prompt, end="")
                line = self.console.input()
            except EOFError:
                line = RichCmd.EOF

            line = self.precmd(line)
            stop = self._onecmd(line)
            stop = self.postcmd(stop, line)

        self.postloop()

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def precmd(self, line: str) -> str:
        return line

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def postcmd(self, stop: bool, line: str) -> bool:
        return stop

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def preloop(self):
        pass

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def postloop(self):
        pass

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def _parseline(self, line: str) -> tuple[None | str, None | str, str]:
        line = line.strip()

        if not line:
            return None, None, line
        elif line[0] == "?":
            line = f"help {line[1:]}"
        elif line[0] == "!":
            if self.supports_shell:
                line = f"shell {line[1:]}"
            else:
                return None, None, line

        i, n = 0, len(line)

        while i < n and line[i] in RichCmd.IDENT_CHARS:
            i = i + 1

        cmd, arg = line[:i], line[i:].strip()
        return cmd, arg, line

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def _onecmd(self, line: str) -> bool:
        cmd, arg, line = self._parseline(line)

        if not line:
            return self.emptyline()

        if cmd is None:
            return self.default(line)

        self._last_command = line

        if line == RichCmd.EOF:
            self._last_command = ""

        if cmd == "":
            return self.default(line)
        else:
            try:
                func = getattr(self, RichCmd.PREFIX_COMMAND + cmd)
            except AttributeError:
                return self.default(line)

            return func(arg)

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def _lastcmd(self) -> bool:
        if self._last_command:
            return self._onecmd(self._last_command)
        return False

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def emptyline(self) -> bool:
        return self._lastcmd()

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def default(self, line: str) -> bool:
        self.console.print(
            RichCmd.MSG_ERR_BAD_COMMAND.format(line),
            style=self.styles["error"]
        )

        return False

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def do_help(self, arg: str):
        """List available commands with "help" or detailed help with "help cmd"."""

        if arg:
            try:
                func = getattr(self, RichCmd.PREFIX_HELP + arg)
            except AttributeError:
                try:
                    doc = getattr(self, RichCmd.PREFIX_COMMAND + arg).__doc__

                    if doc:
                        self.console.print(doc)
                        return
                except AttributeError:
                    pass

                self.console.print(
                    RichCmd.MSG_ERR_NO_HELP.format(arg),
                    style=self.styles["error"]
                )
                return
            func()
        else:
            names = dir(self.__class__)
            cmds_doc = []
            cmds_undoc = []
            misc_help = {}

            for name in names:
                prefix_len = len(RichCmd.PREFIX_HELP)
                if name[:prefix_len] == RichCmd.PREFIX_HELP:
                    misc_help[name[prefix_len:]] = 1

            names.sort()

            # There can be duplicates if routines overridden
            prevname = ""

            for name in names:
                prefix_len = len(RichCmd.PREFIX_COMMAND)
                if name[:prefix_len] == RichCmd.PREFIX_COMMAND:
                    if name == prevname:
                        continue

                    prevname = name
                    cmd = name[prefix_len:]

                    if cmd in misc_help:
                        cmds_doc.append(cmd)
                        del misc_help[cmd]
                    elif getattr(self, name).__doc__:
                        cmds_doc.append(cmd)
                    else:
                        cmds_undoc.append(cmd)

            self.console.print(
                (self.help_header or ""),
                style=self.styles["help-header"]
            )

            self._print_topics(
                self.help_doc,
                cmds_doc,
                self.styles["help-doc"]
            )

            self._print_topics(
                self.help_misc,
                list(misc_help.keys()),
                self.styles["help-misc"]
            )

            self._print_topics(
                self.help_undoc,
                cmds_undoc,
                self.styles["help-undoc"]
            )

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def _print_topics(self, header: str, cmds: list[str], style: str):
        if cmds:
            self.console.print(header, style=style)
            if self.help_ruler:
                self.console.print(self.help_ruler * len(header), style=style)
            self.columnize(cmds, style=style)
            self.console.print()

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def columnize(
            self,
            items: list[str],
            displaywidth: int = None,
            style: str = None
    ):
        displaywidth = (displaywidth or self.console.width) - 1

        if not items:
            self.console.print(RichCmd.MSG_EMPTY, self.styles["warn"])
            return

        nonstrings = [
            i for i in range(len(items)) if not isinstance(items[i], str)
        ]

        if nonstrings:
            offenders = ", ".join(map(str, nonstrings))
            raise TypeError(RichCmd.ERR_NOT_STRING.format(offenders))

        size = len(items)

        if size == 1:
            self.console.print(items[0], style=style)
            return

        # Try every row count from 1 upwards
        for nrows in range(1, len(items)):
            ncols = (size + nrows - 1) // nrows
            colwidths = []
            totwidth = -2

            for col in range(ncols):
                colwidth = 0

                for row in range(nrows):
                    i = row + nrows * col

                    if i >= size:
                        break

                    x = items[i]
                    colwidth = max(colwidth, len(x))

                colwidths.append(colwidth)
                totwidth += colwidth + 2

                if totwidth > displaywidth:
                    break

            if totwidth <= displaywidth:
                break
        else:
            nrows = len(items)
            ncols = 1
            colwidths = [0]

        for row in range(nrows):
            texts = []

            for col in range(ncols):
                i = row + nrows * col
                x = "" if i >= size else items[i]
                texts.append(x)

            while texts and not texts[-1]:
                del texts[-1]

            for col in range(len(texts)):
                texts[col] = texts[col].ljust(colwidths[col])

            self.console.print("  ".join(texts), style=style)
