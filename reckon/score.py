from typing import Any
from .action import Act, Action


class Score:
    """
    A `Score` represents a starting value which has had numerous `Action`s
    applied to it resulting in the current 'total'. All preceding actions are
    available through the 'history' property of the `Score`.
    """

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def __init__(self, start: int = 0):
        """
        Creates a new instance with an optional 'start' value specified. If it
        not then this instance will start at '0'. An `Action` is noted in the
        'history' in order to identify the starting value.

        :param start: An optional starting value
        """

        self._total = start
        self._history = [Action(Act.ASSIGN, start, "Start")]

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @property
    def total(self) -> int:
        """
        The current total of this `Score`.

        :return: The current total
        """

        return self._total

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @property
    def history(self) -> list[Action]:
        """
        A list of all the `Action`s which have been applied to this `Score`.

        :return: A list of all `Action`s
        """

        return self._history[:]

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def __iadd__(self, val: int | tuple[int, str]):
        """
        Adds 'val' to the 'total' of this `Score` and registers an `ADD`
        `Action` for that value.

        :param val: The value to add

        :return: This instance (see docs.python.org)
        """

        val, note = (val[0], val[1]) if isinstance(val, tuple) else (val, None)
        self._total += val
        self._history.append(Action(Act.ADD, val, note))
        return self

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def __isub__(self, val: int | tuple[int, str]):
        """
        Subtracts 'val' from the 'total' of this `Score` and registers a
        `SUBTRACT` `Action` for that value.

        :param val: The value to subtract

        :return: This instance (see docs.python.org)
        """

        value, note = (val[0], val[1]) if isinstance(val, tuple) else (val, None)
        self._total -= value
        self._history.append(Action(Act.SUBTRACT, value, note))
        return self

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def __ior__(self, val: int | tuple[int, str]):
        """
        Sets the 'total' of this `Score` to 'val' and registers an `ASSIGN`
        `Action` for that value.

        :param val:  The value to set

        :return: This instance (see docs.python.org)
        """

        value, note = (val[0], val[1]) if isinstance(val, tuple) else (val, None)
        self._total = value
        self._history.append(Action(Act.ASSIGN, value, note))
        return self

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def __eq__(self, other: Any) -> bool:
        """
        Determins if 'other' is equal to this instance. If 'other' is an `int`
        then it is compared to 'total' otherwise the 'total's of the other
        `Score` and this instance are compared.

        :param other: The other value (`Score` or `int`)

        :return: Whether 'other' is equal to this instance
        """

        if isinstance(other, Score):
            return self._total == other._total
        if isinstance(0, int):
            return self._total == other
        return NotImplemented

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def __gt__(self, other: Any) -> bool:
        """
        Determins if 'other' is greater than this instance. If 'other' is an
        `int` then it is compared to 'total' otherwise the 'total's of the other
        `Score` and this instance are compared.

        :param other: The other value (`Score` or `int`)

        :return: Whether 'other' is greater than this instance
        """

        if isinstance(other, Score):
            return self._total > other._total
        if isinstance(0, int):
            return self._total > other
        return NotImplemented

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def __lt__(self, other: Any) -> bool:
        """
        Determins if 'other' is less than this instance. If 'other' is an `int`
        then it is compared to 'total' otherwise the 'total's of the other
        `Score` and this instance are compared.

        :param other: The other value (`Score` or `int`)

        :return: Whether 'other' is less than this instance
        """

        if isinstance(other, Score):
            return self._total < other._total
        if isinstance(0, int):
            return self._total < other
        return NotImplemented

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def __ge__(self, other: Any) -> bool:
        """
        Determins if 'other' is greater than or equal to this instance. If
        'other' is an `int` then it is compared to 'total' otherwise the
        'total's of the other `Score` and this instance are compared.

        :param other: The other value (`Score` or `int`)

        :return: Whether 'other' is greater than or equal to this instance
        """

        if isinstance(other, Score):
            return self._total >= other._total
        if isinstance(0, int):
            return self._total >= other
        return NotImplemented

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def __le__(self, other: Any) -> bool:
        """
        Determins if 'other' is less than or equal to this instance. If 'other'
        is an `int` then it is compared to 'total' otherwise the 'total's of
        the other `Score` and this instance are compared.

        :param other: The other value (`Score` or `int`)

        :return: Whether 'other' is less than or equal to this instance
        """

        if isinstance(other, Score):
            return self._total <= other._total
        if isinstance(0, int):
            return self._total <= other
        return NotImplemented
